package repository

import (
	"appliers/api-application/internal/model/domain"
	"appliers/api-application/internal/model/dto"
	"context"
	"strings"

	"gorm.io/gorm"
)

type ApplicationRepository interface {
	Save(ctx context.Context, applicant domain.Application) (domain.Application, error)
	Update(ctx context.Context, applicant domain.Application, applicantId uint) (*domain.Application, error)
	Delete(ctx context.Context, applicantId uint) error
	FindById(ctx context.Context, applicantId uint) (*domain.Application, error)
	FindAll(ctx context.Context, payload *dto.SearchGetRequest, p *dto.Pagination) ([]domain.Application, *dto.PaginationInfo, error)
	CountById(ctx context.Context, applicantId uint) (int, error)
}

type ApplicationRepositoryImplSql struct {
	DB *gorm.DB
}

func NewApplicationRepositoryImplSql(db *gorm.DB) ApplicationRepository {
	return &ApplicationRepositoryImplSql{
		DB: db,
	}
}

func (repository *ApplicationRepositoryImplSql) Save(ctx context.Context, applicant domain.Application) (domain.Application, error) {
	err := repository.DB.WithContext(ctx).Save(&applicant).Error
	return applicant, err
}

func (repository *ApplicationRepositoryImplSql) Update(ctx context.Context, applicant domain.Application, applicantId uint) (*domain.Application, error) {
	err := repository.DB.WithContext(ctx).Model(&applicant).Where("id = ?", applicantId).Updates(&applicant).Error
	updatedUser := domain.Application{}
	repository.DB.WithContext(ctx).First(&updatedUser, applicantId)
	return &updatedUser, err
}

func (repository *ApplicationRepositoryImplSql) Delete(ctx context.Context, applicantId uint) error {
	err := repository.DB.WithContext(ctx).Delete(&domain.Application{}, applicantId).Error
	if err == nil {
		applicant := domain.Application{}
		err = repository.DB.WithContext(ctx).Unscoped().Model(&applicant).Where("id = ?", applicantId).Updates(&applicant).Error
	}
	return err
}

func (repository *ApplicationRepositoryImplSql) FindById(ctx context.Context, applicantId uint) (*domain.Application, error) {
	var applicant = domain.Application{}
	err := repository.DB.WithContext(ctx).First(&applicant, applicantId).Error
	if err == nil {
		return &applicant, nil
	} else {
		return nil, err
	}
}

func (repository *ApplicationRepositoryImplSql) FindAll(ctx context.Context, payload *dto.SearchGetRequest, p *dto.Pagination) ([]domain.Application, *dto.PaginationInfo, error) {
	var applicants []domain.Application
	var count int64

	query := repository.DB.WithContext(ctx).Model(&domain.Application{})

	if payload.Search != "" {
		search := "%" + strings.ToLower(payload.Search) + "%"
		query = query.Where("lower(fullname) LIKE ?", search)
	}

	countQuery := query
	if err := countQuery.Count(&count).Error; err != nil {
		return nil, nil, err
	}

	limit, offset := dto.GetLimitOffset(p)

	err := query.Limit(limit).Offset(offset).Find(&applicants).Error

	return applicants, dto.CheckInfoPagination(p, count), err
}

func (repository *ApplicationRepositoryImplSql) CountById(ctx context.Context, applicantId uint) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&domain.Application{}).Where("id = ?", applicantId).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}
