package factory

import (
	"appliers/api-application/internal/repository"

	"gorm.io/gorm"
)

type ApplicationFactory struct {
	ApplicationRepository repository.ApplicationRepository
}

func NewApplicationFactory(db *gorm.DB) *ApplicationFactory {
	return &ApplicationFactory{
		ApplicationRepository: repository.NewApplicationRepositoryImplSql(db),
	}
}
