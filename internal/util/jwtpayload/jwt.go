package jwtpayload

import (
	"appliers/api-application/pkg/constant"
	"os"

	"github.com/golang-jwt/jwt"
)

type JWTPayload struct {
	IsFromAnotherService bool
	UserID               uint
	Username             string
	Role                 constant.UserRole
	PersonId             int64
	InstitutionId        int64
}

func NewJWTPayload(token *jwt.Token, authToken *string) *JWTPayload {
	if token != nil {
		return &JWTPayload{
			IsFromAnotherService: false,
			UserID:               uint(token.Claims.(jwt.MapClaims)["id"].(float64)),
			Username:             token.Claims.(jwt.MapClaims)["username"].(string),
			Role:                 constant.UserRole(token.Claims.(jwt.MapClaims)["role"].(string)),
			PersonId:             int64(token.Claims.(jwt.MapClaims)["person"].(float64)),
			InstitutionId:        int64(token.Claims.(jwt.MapClaims)["institution"].(float64)),
		}
	}

	if token == nil && *authToken == os.Getenv("RANDOM_KEY") {
		return &JWTPayload{
			IsFromAnotherService: true,
		}
	}

	return nil
}
