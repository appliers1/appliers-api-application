package compare

import (
	"appliers/api-application/internal/util/jwtpayload"
	"appliers/api-application/pkg/constant"
	"context"
)

func CompareCompanyID(ctx context.Context, companyId uint) bool {
	// get jwt payload
	cc := ctx.Value(constant.CONTEXT_JWT_PAYLOAD_KEY).(*jwtpayload.JWTPayload)
	// Pass if user's company equal company data value
	if cc.Role == constant.ROLE_COMPANY && uint(cc.InstitutionId) != companyId {
		return false
	}
	return true
}
