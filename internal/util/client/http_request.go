package client

import (
	"appliers/api-application/pkg/util/response"
	"encoding/json"
	"io"
	"net/http"
	"os"
	"time"
)

type ClientHttpRequest[T any] struct {
	Url                 string
	Method              string
	Payload             io.Reader
	ContentType         string
	Timeout             time.Duration
	Data                T
	IgnoreErrorResponse bool
}

type SuccessResponse[T any] struct {
	Meta response.Meta `json:"meta"`
	Data T             `json:"data"`
}

type ErrorResponse struct {
	Meta  response.Meta `json:"meta"`
	Error string        `json:"error"`
}

func (c *ClientHttpRequest[T]) StandardRequest() (*T, error) {
	var client = &http.Client{Timeout: c.Timeout}
	request, err := http.NewRequest(c.Method, c.Url, c.Payload)
	if err != nil {
		return nil, err
	}

	request.Header.Set("content-type", c.ContentType)
	request.Header.Set("Authorization", os.Getenv("RANDOM_KEY"))

	resp, err := client.Do(request)

	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == 200 {
		data := SuccessResponse[T]{}
		err = json.NewDecoder(resp.Body).Decode(&data)
		if err != nil {
			return nil, err
		}
		return &data.Data, err // Return Data Here
	} else {
		data := ErrorResponse{}
		err = json.NewDecoder(resp.Body).Decode(&data)
		if err != nil {
			return nil, err
		}
	}

	return nil, err
}
