package entity

type Experience struct {
	CompanyName string `json:"company_name"`
	Position    string `json:"position"`
	Jobdesk     string `json:"jobdesk"`
	StartDate   string `json:"start_date"`
	EndDate     string `json:"end_date"`
}
