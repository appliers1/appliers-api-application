package entity

type Education struct {
	ApplicantID uint   `json:"applicant_id"`
	DegreeType  string `json:"degree_type" sql:"type:degree_type"`
	Name        string `json:"name"`
}
