package entity

type Applicant struct {
	ID           uint         `json:"id"`
	UserID       uint         `json:"user_id"`
	FullName     string       `json:"fullname"`
	PlaceOfBirth string       `json:"place_of_birth"`
	DateOfBirth  string       `json:"date_of_birth"`
	Gender       string       `json:"gender"`
	Photo        string       `json:"photo"`
	Phone        string       `json:"phone"`
	Address      string       `json:"address"`
	PostalCode   string       `json:"postal_code"`
	Educations   []Education  `json:"educations"`
	Experiences  []Experience `json:"experiences"`
}
