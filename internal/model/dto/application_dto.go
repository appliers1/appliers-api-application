package dto

import (
	"appliers/api-application/internal/model/entity"
	"appliers/api-application/pkg/constant"
)

/* REQUEST */
type ApplicationRequestBody struct {
	JobID       uint                    `json:"job_id" validate:"required"`
	CompanyID   uint                    `json:"company_id" validate:"required"`
	UserID      uint                    `json:"user_id" validate:"required"`
	ApplicantID uint                    `json:"applicant_id" validate:"required"`
	CV          string                  `json:"cv"`
	Message     string                  `json:"message"`
	Stage       constant.StageInterview `json:"stage"`
}

type UpdateApplicationRequestBody struct {
	CompanyID uint  `json:"company_id" validate:"required"`
	Stage     int32 `json:"stage"`
}

/* RESPONSE */
type ApplicationResponse struct {
	JobID       uint   `json:"job_id"`
	ApplicantID uint   `json:"applicant_id"`
	CompanyID   uint   `json:"company_id"`
	CV          string `json:"cv"`
	Message     string `json:"message"`
	Stage       string `json:"stage"`
	CreatedAt   string `json:"created_at"`
}

/* RESPONSE */
type ApplicationDetailResponse struct {
	JobID       uint              `json:"job_id"`
	ApplicantID uint              `json:"applicant_id"`
	Applicant   *entity.Applicant `json:"applicant"`
	CompanyID   uint              `json:"company_id"`
	CV          string            `json:"cv"`
	Message     string            `json:"message"`
	Stage       string            `json:"stage"`
	CreatedAt   string            `json:"created_at"`
}
