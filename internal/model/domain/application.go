package domain

import (
	"appliers/api-application/pkg/constant"
	"time"

	"gorm.io/gorm"
)

type Application struct {
	gorm.Model
	JobID       uint                    `json:"job_id" gorm:"not null"`
	CompanyID   uint                    `json:"company_id" gorm:"not null"`
	ApplicantID uint                    `json:"applicant_id" gorm:"not null"`
	UserID      uint                    `json:"user_id" gorm:"not null"`
	CV          string                  `json:"cv" gorm:"size:100;not null"`
	Message     string                  `json:"message" gorm:"size:500;not null"`
	Stage       constant.StageInterview `json:"stage" sql:"type:stage" gorm:"type:enum('1','2','3','4','5');not null"`
}

// BeforeCreate is a method for struct User
// gorm call this method before they execute query
func (u *Application) BeforeCreate(tx *gorm.DB) (err error) {
	u.CreatedAt = time.Now()
	return
}

// BeforeUpdate is a method for struct User
// gorm call this method before they execute query
func (u *Application) BeforeUpdate(tx *gorm.DB) (err error) {
	u.UpdatedAt = time.Now()
	return
}
