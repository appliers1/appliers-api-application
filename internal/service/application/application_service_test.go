package application

import (
	"appliers/api-application/internal/factory"
	_mockRepository "appliers/api-application/internal/mocks/repository"
	"appliers/api-application/internal/model/domain"
	"appliers/api-application/internal/model/dto"
	"appliers/api-application/internal/util/jwtpayload"
	"appliers/api-application/pkg/constant"
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var applicationRepository _mockRepository.ApplicationRepository

var ctx context.Context
var applicationService ApplicationService
var applicationFactory = factory.ApplicationFactory{
	ApplicationRepository: &applicationRepository,
}
var applicationDomain domain.Application
var err error

func setup() {
	applicationService = NewService(&applicationFactory)
	payload := jwtpayload.JWTPayload{
		UserID:        1,
		Role:          constant.ROLE_COMPANY,
		InstitutionId: 1,
	}
	ctx = context.Background()
	ctx = context.WithValue(ctx, constant.CONTEXT_JWT_PAYLOAD_KEY, &payload)
	err = errors.New("mock")
	applicationDomain = domain.Application{
		JobID:       1,
		CompanyID:   1,
		ApplicantID: 1,
		UserID:      1,
		CV:          "url",
		Message:     "aa",
		Stage:       constant.Accepted,
	}
}

func TestDelete(t *testing.T) {
	setup()
	applicationRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	applicationRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 1 | Valid Delete", func(t *testing.T) {
		err := applicationService.Delete(ctx, uint(1))
		assert.Nil(t, err)
	})
}

func TestDeleteFail(t *testing.T) {
	setup()
	applicationRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, nil).Once()
	applicationRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 2 | Invalid Delete", func(t *testing.T) {
		err := applicationService.Delete(ctx, uint(1))
		assert.NotNil(t, err)
	})

}

func TestFindById(t *testing.T) {
	setup()
	applicationRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	applicationRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&applicationDomain, nil).Once()

	t.Run("Test Case 5 | Valid FindById", func(t *testing.T) {
		applicant, _ := applicationService.FindById(context.Background(), uint(1))
		assert.Equal(t, applicant.Message, applicationDomain.Message)
	})
}

func TestFindByIdFail(t *testing.T) {
	setup()
	applicationRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, nil).Once()
	applicationRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 6 | Invalid FindById", func(t *testing.T) {
		_, err := applicationService.FindById(context.Background(), uint(1))
		assert.NotNil(t, err)
	})
}

func TestUpdate(t *testing.T) {
	setup()
	applicationUpdate := domain.Application{
		JobID:       1,
		CompanyID:   2,
		ApplicantID: 1,
		UserID:      1,
		CV:          "url",
		Message:     "aa",
		Stage:       constant.Rejected,
	}
	dto := dto.UpdateApplicationRequestBody{
		CompanyID: applicationUpdate.CompanyID,
		Stage:     int32(applicationUpdate.Stage),
	}

	applicationRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	applicationRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&applicationUpdate, nil).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		applicant, _ := applicationService.Update(ctx, &dto, uint(1))
		assert.NotEqual(t, applicant.Stage, applicationDomain.Stage)
	})
}

func TestUpdateFail(t *testing.T) {
	setup()
	applicationUpdate := domain.Application{
		JobID:       1,
		CompanyID:   2,
		ApplicantID: 1,
		UserID:      1,
		CV:          "url",
		Message:     "aa",
		Stage:       constant.Rejected,
	}
	dto := dto.UpdateApplicationRequestBody{
		CompanyID: applicationUpdate.CompanyID,
		Stage:     int32(applicationUpdate.Stage),
	}

	applicationRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	applicationRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 3 | Invalid Update", func(t *testing.T) {
		_, err := applicationService.Update(ctx, &dto, uint(1))
		assert.NotNil(t, err)
	})
}

func TestCreate(t *testing.T) {
	setup()
	dto := dto.ApplicationRequestBody{
		JobID:       applicationDomain.JobID,
		CompanyID:   applicationDomain.CompanyID,
		UserID:      applicationDomain.UserID,
		CV:          applicationDomain.CV,
		Stage:       applicationDomain.Stage,
		ApplicantID: applicationDomain.ApplicantID,
		Message:     applicationDomain.Message,
	}
	applicationRepository.On("Save",
		mock.Anything,
		mock.Anything).Return(applicationDomain, nil).Once()
	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {

		application, _ := applicationService.Create(ctx, &dto)
		assert.Equal(t, application.CompanyID, applicationDomain.CompanyID)
	})
}

func TestCreateFail(t *testing.T) {
	setup()
	dto := dto.ApplicationRequestBody{
		JobID:       applicationDomain.JobID,
		CompanyID:   applicationDomain.CompanyID,
		UserID:      applicationDomain.UserID,
		CV:          applicationDomain.CV,
		Stage:       applicationDomain.Stage,
		ApplicantID: applicationDomain.ApplicantID,
		Message:     applicationDomain.Message,
	}
	applicationRepository.On("Save",
		mock.Anything,
		mock.Anything).Return((domain.Application{}), err).Once()

	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {
		_, err := applicationService.Create(ctx, &dto)
		assert.NotNil(t, err)
	})
}

func TestFind(t *testing.T) {
	setup()
	var data []domain.Application
	data = append(data, applicationDomain)
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	applicationRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(data, dto.CheckInfoPagination(&pagination, 10), nil).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "aa",
		}
		applicants, _ := applicationService.Find(context.Background(), &dto)
		assert.Equal(t, applicants.Data[0].CompanyID, applicationDomain.CompanyID)
	})
}

func TestFindFail(t *testing.T) {
	setup()
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	applicationRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(nil, nil, err).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "aa",
		}
		_, err := applicationService.Find(context.Background(), &dto)
		assert.NotNil(t, err)
	})
}
