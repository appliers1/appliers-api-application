package application

import (
	"appliers/api-application/internal/model/domain"
	"appliers/api-application/internal/model/dto"
	"appliers/api-application/pkg/constant"
)

func ToApplicationResponse(application domain.Application) dto.ApplicationResponse {
	// Versi simple , gk fetch data dati applicant service
	return dto.ApplicationResponse{
		JobID:       application.JobID,
		ApplicantID: application.ApplicantID,
		CompanyID:   application.CompanyID,
		CV:          application.CV,
		Message:     application.Message,
		Stage:       application.Stage.String(),
		CreatedAt:   application.CreatedAt.String(),
	}
}

func ToApplicationDetailResponse(application domain.Application, h ApplicantHttpRequest) dto.ApplicationDetailResponse {
	applicant, err := h.FetchApplicant(application.ApplicantID)
	if err != nil {
		applicant = nil
	}

	return dto.ApplicationDetailResponse{
		JobID:       application.JobID,
		ApplicantID: application.ApplicantID,
		CompanyID:   application.CompanyID,
		Applicant:   applicant,
		CV:          application.CV,
		Message:     application.Message,
		Stage:       application.Stage.String(),
		CreatedAt:   application.CreatedAt.String(),
	}
}

func ToApplicationDomain(req *dto.ApplicationRequestBody) domain.Application {
	application := domain.Application{
		JobID:       req.JobID,
		ApplicantID: req.ApplicantID,
		CompanyID:   req.CompanyID,
		UserID:      req.UserID,
		CV:          req.CV,
		Message:     req.Message,
		Stage:       req.Stage,
	}
	return application
}

func UpdateToApplicationDomain(req *dto.UpdateApplicationRequestBody) domain.Application {
	application := domain.Application{}

	if req.Stage != 0 {
		application.Stage = constant.NewStageInterview(&req.Stage)
	}

	return application
}
