package application

import (
	"appliers/api-application/internal/factory"
	"appliers/api-application/internal/model/dto"
	"context"
	"fmt"

	repository "appliers/api-application/internal/repository"
	res "appliers/api-application/pkg/util/response"
)

type ApplicationService interface {
	Create(ctx context.Context, payload *dto.ApplicationRequestBody) (*dto.ApplicationResponse, error)
	Update(ctx context.Context, payload *dto.UpdateApplicationRequestBody, applicationId uint) (*dto.ApplicationResponse, error)
	// UpdateStage(ctx context.Context, applicationId uint, stage string) (*dto.ApplicationResponse, error)
	Delete(ctx context.Context, applicationId uint) error
	FindById(ctx context.Context, applicationId uint) (*dto.ApplicationDetailResponse, error)
	Find(ctx context.Context, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.ApplicationResponse], error)
}

type service struct {
	ApplicationRepository repository.ApplicationRepository
	ApplicantHttpRequest  ApplicantHttpRequest
}

func NewService(f *factory.ApplicationFactory) ApplicationService {
	return &service{
		ApplicationRepository: f.ApplicationRepository,
		ApplicantHttpRequest:  NewHttpRequestToApplicant(),
	}
}

func (s *service) Create(ctx context.Context, payload *dto.ApplicationRequestBody) (*dto.ApplicationResponse, error) {
	fmt.Println(payload)
	data, err := s.ApplicationRepository.Save(ctx, ToApplicationDomain(payload))

	if err != nil {
		return nil, err
	} else {
		result := ToApplicationResponse(data)
		return &result, nil
	}
}

func (s *service) Update(ctx context.Context, payload *dto.UpdateApplicationRequestBody, applicationId uint) (*dto.ApplicationResponse, error) {
	count, err := s.ApplicationRepository.CountById(ctx, applicationId)
	if err == nil {
		if count == 1 {

			data, err := s.ApplicationRepository.Update(ctx, UpdateToApplicationDomain(payload), applicationId)

			if err == nil {
				data.ID = applicationId
				applicantResponse := ToApplicationResponse(*data)
				return &applicantResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)

}

func (s *service) Delete(ctx context.Context, applicationId uint) error {
	count, err := s.ApplicationRepository.CountById(ctx, applicationId)
	if err == nil {
		if count == 1 {
			err := s.ApplicationRepository.Delete(ctx, applicationId)
			if err == nil {
				return nil
			}
		} else {
			if count == 0 {
				return res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) FindById(ctx context.Context, applicationId uint) (*dto.ApplicationDetailResponse, error) {
	count, err := s.ApplicationRepository.CountById(ctx, applicationId)
	if err == nil {
		if count == 1 {
			applicant, err := s.ApplicationRepository.FindById(ctx, applicationId)
			if err == nil {
				applicantResponse := ToApplicationDetailResponse(*applicant, s.ApplicantHttpRequest)
				return &applicantResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Find(ctx context.Context, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.ApplicationResponse], error) {

	applicants, info, err := s.ApplicationRepository.FindAll(ctx, payload, &payload.Pagination)
	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	}

	var data []dto.ApplicationResponse

	for _, applicant := range applicants {
		data = append(data, ToApplicationResponse(applicant))
	}

	result := new(dto.SearchGetResponse[dto.ApplicationResponse])
	result.Data = data
	result.PaginationInfo = *info

	return result, nil
}
