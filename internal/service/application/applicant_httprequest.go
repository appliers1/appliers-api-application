package application

import (
	"appliers/api-application/internal/model/entity"
	"appliers/api-application/internal/util/client"
	"appliers/api-application/pkg/constant"
	"net/http"
	"strconv"
	"time"
)

type ApplicantHttpRequest interface {
	FetchApplicant(applicantId uint) (*entity.Applicant, error)
}

type httpRequestToApplicant struct {
	BaseUrl string
}

func NewHttpRequestToApplicant() ApplicantHttpRequest {
	return &httpRequestToApplicant{
		BaseUrl: constant.ApplicantServiceUrl,
	}
}

func (h *httpRequestToApplicant) FetchApplicant(applicantId uint) (*entity.Applicant, error) {
	var applicantID string = strconv.Itoa(int(applicantId))
	httpRequest := client.ClientHttpRequest[entity.Applicant]{
		Url:         h.BaseUrl + "/applicants/" + applicantID,
		Method:      http.MethodGet,
		Payload:     nil,
		ContentType: constant.ContentTypeApplicationJson,
		Timeout:     10 * time.Second,
	}

	applicant, err := httpRequest.StandardRequest()
	return applicant, err
}
