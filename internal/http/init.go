package http

import (
	"appliers/api-application/internal/factory"
	c "appliers/api-application/internal/http/controller"
	"appliers/api-application/internal/http/router"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

func Init(e *echo.Echo, db *gorm.DB) {
	router.ApplicationRouter(e, c.NewApplicationController(factory.NewApplicationFactory(db)))
}
