package controller

import (
	"appliers/api-application/internal/factory"
	"appliers/api-application/internal/model/dto"
	service "appliers/api-application/internal/service/application"
	"appliers/api-application/internal/util/jwtpayload"
	"strconv"

	"appliers/api-application/pkg/constant"
	res "appliers/api-application/pkg/util/response"

	"github.com/labstack/echo/v4"
)

type ApplicationController struct {
	ApplicationService service.ApplicationService
}

func NewApplicationController(f *factory.ApplicationFactory) *ApplicationController {
	return &ApplicationController{
		ApplicationService: service.NewService(f),
	}
}

func (controller *ApplicationController) Create(c echo.Context) error {
	payload := new(dto.ApplicationRequestBody)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	cc := c.Request().Context().Value(constant.CONTEXT_JWT_PAYLOAD_KEY).(*jwtpayload.JWTPayload)
	if cc.Role == constant.ROLE_APPLICANT && cc.PersonId != int64(0) {
		payload.Stage = constant.Unread
		payload.UserID = cc.UserID
		payload.ApplicantID = uint(cc.PersonId)

		application, err := controller.ApplicationService.Create(c.Request().Context(), payload)
		if err != nil {
			return res.ErrorResponse(err).Send(c)
		}
		return res.SuccessResponse(application).Send(c)
	}
	return res.ErrorBuilder(&res.ErrorConstant.Unauthorized, nil)
}

// func (controller *ApplicationController) Update(c echo.Context) error {
// 	payload := new(dto.UpdateApplicationRequestBody)
// 	err := c.Bind(payload)
// 	id, errId := strconv.ParseUint(c.Param("id"), 10, 32)
// 	if err != nil || errId != nil {
// 		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
// 	}

// 	cc := c.Request().Context().Value(constant.CONTEXT_JWT_PAYLOAD_KEY).(*jwtpayload.JWTPayload)
// 	if (cc.Role == constant.ROLE_COMPANY && cc.InstitutionId == int64(payload.CompanyID)) || cc.Role == constant.ROLE_ADMIN {
// 		application, err := controller.ApplicationService.Update(c.Request().Context(), payload, uint(id))
// 		if err != nil {
// 			return res.ErrorResponse(err).Send(c)
// 		}
// 		return res.SuccessResponse(application).Send(c)
// 	}
// 	return res.ErrorBuilder(&res.ErrorConstant.Unauthorized, nil)
// }

// func (controller *ApplicationController) Delete(c echo.Context) error {
// 	id, err := strconv.ParseUint(c.Param("id"), 10, 32)
// 	if err != nil {
// 		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
// 	}

// 	err = controller.ApplicationService.Delete(c.Request().Context(), uint(id))
// 	if err != nil {
// 		return res.ErrorResponse(err).Send(c)
// 	}
// 	return res.SuccessResponse(nil).Send(c)
// }

func (controller *ApplicationController) FindById(c echo.Context) error {
	var id, err = strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	application, err := controller.ApplicationService.FindById(c.Request().Context(), uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}

	cc := c.Request().Context().Value(constant.CONTEXT_JWT_PAYLOAD_KEY).(*jwtpayload.JWTPayload)
	// fmt.Println("InstiID", cc.InstitutionId, "Comp ID : ", int64(application.CompanyID))
	if cc.Role == constant.ROLE_COMPANY && cc.InstitutionId == int64(application.CompanyID) {

		// TODO : Fetch Applicant Data
		// TODO : Update Stage to 2

		return res.SuccessResponse(application).Send(c)
	}
	return res.ErrorBuilder(&res.ErrorConstant.Unauthorized, err).Send(c)
}

func (controller *ApplicationController) Find(c echo.Context) error {
	payload := new(dto.SearchGetRequest)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	if err := c.Validate(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.Validation, err).Send(c)
	}

	result, err := controller.ApplicationService.Find(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(result.Data).Send(c)
}

func (controller *ApplicationController) UpdateStage(c echo.Context) error {
	var id, err = strconv.ParseUint(c.Param("id"), 10, 32)
	var stageId, errS = strconv.ParseInt(c.Param("stage_id"), 10, 32)
	if err != nil || errS != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	payload := &dto.UpdateApplicationRequestBody{
		CompanyID: uint(id),
		Stage:     int32(stageId),
	}
	application, err := controller.ApplicationService.Update(c.Request().Context(), payload, uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(application).Send(c)
}
