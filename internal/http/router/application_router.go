package router

import (
	"appliers/api-application/internal/http/controller"
	"appliers/api-application/internal/http/middleware"
	. "appliers/api-application/pkg/constant"

	"github.com/labstack/echo/v4"
)

func ApplicationRouter(e *echo.Echo, c *controller.ApplicationController) {
	// FIX
	// e.GET("/applications", c.Find, middleware.NewAllowedRole(AllRole).Authentication) // Untuk Filterny
	e.POST("/applications", c.Create, middleware.NewAllowedRole(map[UserRole]bool{ROLE_APPLICANT: true}).Authentication)

	e.GET("/applications/:id", c.FindById, middleware.NewAllowedRole(map[UserRole]bool{ROLE_COMPANY: true}).Authentication)
	e.PATCH("/applications/:id/stage/:stage_id", c.UpdateStage, middleware.NewAllowedRole(map[UserRole]bool{ROLE_COMPANY: true}).Authentication)

	e.GET("/applications/jobs/:jobs_id", c.Find, middleware.NewAllowedRole(map[UserRole]bool{ROLE_COMPANY: true}).Authentication)
	e.GET("/applications/jobs/:jobs_id/applicant/:applicant_id", c.Find, middleware.NewAllowedRole(map[UserRole]bool{ROLE_COMPANY: true}).Authentication)

	// e.DELETE("/applications/:id", c.Update, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
}
