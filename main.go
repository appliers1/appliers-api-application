package main

import (
	"appliers/api-application/database"
	"appliers/api-application/database/migration"
	"appliers/api-application/internal/http"
	"appliers/api-application/internal/http/middleware"
	"flag"
	"os"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
)

// load env configuration
func init() {
	if err := godotenv.Load(); err != nil {
		panic(err)
	}
}

func main() {

	database.CreateConnection()
	var m string // for check migration

	flag.StringVar(
		&m,
		"migrate",
		"run",
		`this argument for check if user want to migrate table, rollback table, or status migration

to use this flag:
	use -migrate=migrate for migrate table
	use -migrate=rollback for rollback table
	use -migrate=status for get status migration`,
	)
	flag.Parse()

	if m == "migrate" {
		migration.Migrate()
		return
	} else if m == "rollback" {
		migration.Rollback()
		return
	} else if m == "status" {
		migration.Status()
		return
	}
	db := database.GetConnection()
	e := echo.New()

	middleware.Init(e)
	http.Init(e, db)

	e.Logger.Fatal(e.Start(":" + os.Getenv("APP_PORT")))
}
