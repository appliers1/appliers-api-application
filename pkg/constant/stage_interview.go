package constant

import "fmt"

type StageInterview int32

func NewStageInterview(stage *int32) StageInterview {
	switch *stage {
	default:
		return Unread
	case 2:
		return OnReview
	case 3:
		return Interview
	case 4:
		return Accepted
	case 5:
		return Rejected
	}
}

const (
	Unread    StageInterview = 1
	OnReview  StageInterview = 2
	Interview StageInterview = 3
	Accepted  StageInterview = 4
	Rejected  StageInterview = 5
)

func (e StageInterview) String() string {
	switch e {
	case Unread:
		return "Unread"
	case OnReview:
		return "On Review"
	case Interview:
		return "Interview"
	case Accepted:
		return "Accepted"
	case Rejected:
		return "Rejected"
	default:
		return fmt.Sprintf("%d", int(e))
	}
}
