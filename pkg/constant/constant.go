package constant

import (
	"fmt"

	"gorm.io/gorm"
)

var BaseUrl = "http://13.250.105.254" // API Ingress nya

var (
	UserServiceUrl        = BaseUrl // + ":8001"
	CompanyServiceUrl     = BaseUrl // + ":8002"
	ApplicantServiceUrl   = BaseUrl // + ":8003"
	JobServiceUrl         = BaseUrl // + ":8004"
	ApplicationServiceUrl = BaseUrl // + ":8005"
)

const ContentTypeApplicationJson = "application/json"

const CONTEXT_JWT_PAYLOAD_KEY = "jwt_payload_key"

var (
	RecordNotFound = gorm.ErrRecordNotFound
)

type UserRole string
type UserStatus string

type Gender string

const (
	MALE   Gender = "male"
	FEMALE Gender = "female"
)

type DegreeType int32

const (
	Elementary   DegreeType = 1
	MiddleSchool DegreeType = 2
	HighSchool   DegreeType = 3
	Associate    DegreeType = 4
	Bachelor     DegreeType = 5
	Master       DegreeType = 6
	Doctor       DegreeType = 7
)

func (e DegreeType) String() string {
	switch e {
	case Elementary:
		return "Sekolah Dasar"
	case MiddleSchool:
		return "Sekolah Menengah Pertama"
	case HighSchool:
		return "Sekolah Menengah Atas"
	case Associate:
		return "Diploma"
	case Bachelor:
		return "S1"
	case Master:
		return "S2"
	case Doctor:
		return "S3"
	default:
		return fmt.Sprintf("%d", int(e))
	}
}

var AllRole = map[UserRole]bool{
	ROLE_ADMIN:     true,
	ROLE_APPLICANT: true,
	ROLE_COMPANY:   true,
}

const (
	ROLE_ADMIN     UserRole = "admin"
	ROLE_APPLICANT UserRole = "applicant"
	ROLE_COMPANY   UserRole = "company"
)

const (
	STATUS_ACTIVE    UserStatus = "active"
	STATUS_PENDING   UserStatus = "pending"
	STATUS_SUSPENDED UserStatus = "suspended"
	STATUS_BANNED    UserStatus = "banned"
)
